"use strict"

function lightKey () {
    const btnActiveEnter = document.querySelector('.enter');
    const btnActiveS = document.querySelector('.s');    
    const btnActiveE = document.querySelector('.e');
    const btnActiveO = document.querySelector('.o');
    const btnActiveN = document.querySelector('.n');
    const btnActiveL = document.querySelector('.l');
    const btnActiveZ = document.querySelector('.z');

    
    window.addEventListener('keydown', (element) => {
        if (element.code === "Enter") {
            btnActiveEnter.classList.toggle('active');
            btnActiveS.classList.remove('active');
            btnActiveE.classList.remove('active');
            btnActiveO.classList.remove('active');
            btnActiveN.classList.remove('active');
            btnActiveL.classList.remove('active');
            btnActiveZ.classList.remove('active');
        }
        if (element.code === "KeyS") {
            btnActiveEnter.classList.remove('active');
            btnActiveS.classList.toggle('active');
            btnActiveE.classList.remove('active');
            btnActiveO.classList.remove('active');
            btnActiveN.classList.remove('active');
            btnActiveL.classList.remove('active');
            btnActiveZ.classList.remove('active');
        }
        if (element.code === "KeyE") {
            btnActiveEnter.classList.remove('active');
            btnActiveS.classList.remove('active');
            btnActiveE.classList.toggle('active');
            btnActiveO.classList.remove('active');
            btnActiveN.classList.remove('active');
            btnActiveL.classList.remove('active');
            btnActiveZ.classList.remove('active');
        }
        if (element.code === "KeyO") {
            btnActiveEnter.classList.remove('active');
            btnActiveS.classList.remove('active');
            btnActiveE.classList.remove('active');
            btnActiveO.classList.toggle('active');
            btnActiveN.classList.remove('active');
            btnActiveL.classList.remove('active');
            btnActiveZ.classList.remove('active');
        }
        if (element.code === "KeyN") {
            btnActiveEnter.classList.remove('active');
            btnActiveS.classList.remove('active');
            btnActiveE.classList.remove('active');
            btnActiveO.classList.remove('active');
            btnActiveN.classList.toggle('active');
            btnActiveL.classList.remove('active');
            btnActiveZ.classList.remove('active');
        }
        if (element.code === "KeyL") {
            btnActiveEnter.classList.remove('active');
            btnActiveS.classList.remove('active');
            btnActiveE.classList.remove('active');
            btnActiveO.classList.remove('active');
            btnActiveN.classList.remove('active');
            btnActiveL.classList.toggle('active');
            btnActiveZ.classList.remove('active');
        }
        if (element.code === "KeyZ") {
            btnActiveEnter.classList.remove('active');
            btnActiveS.classList.remove('active');
            btnActiveE.classList.remove('active');
            btnActiveO.classList.remove('active');
            btnActiveN.classList.remove('active');
            btnActiveL.classList.remove('active');
            btnActiveZ.classList.toggle('active');
        }
    });
}
lightKey();