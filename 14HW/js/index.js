"use strict"

const changeTheme = document.querySelector(".btn");
const bodyPage = document.querySelector(".page");
const headerlogo = document.querySelector(".page-header-logo");
const pageContainer = document.querySelector(".container");
const pageSidebar = document.querySelector(".blog-sidebar");
const sidebarItem = document.querySelectorAll(".sidebar-item-link");
const imgBorder = document.querySelector(".image-border");
const blogText = document.querySelectorAll(".blog-text");
const footerLink = document.querySelectorAll(".footer-menu-link");
let theme = localStorage.getItem('theme');

function toggleTheme() {
  if (bodyPage.classList.contains('stars')) {
    bodyPage.classList.remove('stars');
    theme = "";
  } else {
    bodyPage.classList.add('stars');
    theme = 'stars';
  }
  
  localStorage.setItem('theme', theme); 
}

  changeTheme.addEventListener('click', () => 'toggleTheme');
