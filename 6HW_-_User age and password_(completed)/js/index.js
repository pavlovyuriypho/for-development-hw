"use strict"

function createNewUser() {
  let newUser = {
    firstName: prompt('Напишите Ваше имя?'),
    lastName: prompt('Напишите Вашу фамилию?'),
    birthday: prompt('Напишите дату Вашего рождения dd.mm.yyyy.').split('.'),

    newDate: new Date(newUser.birthday),    
      
    getAge: function () {
      return((Date.now() - this.newDate) / (1000 * 3600 * 24 * 365))
    },
    getPassword: function () {
      return(this.firstName[0] + this.lastName.toLowerCase() + this.birthday.slice(6,10))
    },
    getLogin: function () {
      return(this.firstName[0] + this.lastName)
    }
  }
  return(newUser);
}
let user = createNewUser();

console.log(user.getAge());
console.log(user.getPassword());